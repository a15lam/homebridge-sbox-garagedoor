# homebridge-sbox-garagedoor
[Homebridge](https://github.com/nfarina/homebridge) plugin that supports custom DIY garage door opener.

This project is inspired by https://github.com/apexad/homebridge-garagedoor-command

## Installation

1. Install homebridge using: `npm install -g homebridge`
2. Install this plugin using: `npm install -g homebridge-sbox-garagedoor`
3. Update your configuration file. See the sample below.

## Configuration

Configuration sample:

```json
"accessories": [
  {
    "accessory": "SboxGarageDoor",
    "name": "Garage Door",
    "open_url": "http://localhost/open.php",
    "close_url": "http://localhost/close.php",
    "state_url": "http://localhost/state.php",
    "status_update_delay": 15
  }
]

```

## How it works

Even though I created this plugin for my personal DIY garage door openner, you can still use it for your own purpose. You just need to make sure of the followings...

01. When you open your Home app it will make a GET call to the URL specified in the 'state_url' config. Response from this URL must be of status code 200 and the body should contain ```{"state":0}``` for closed state or ```{"state":1}``` for open state.

02. When you open your garage door using the Home app it will make a GET call to the URL specified in the 'open_url' config. Response from this URL must be of status code 200 and the body should contain ```{"state":1}``` for open state (or ```{"state":0}``` for closed state in case of failure).

03. When you close your garage door using the Home app it will make a GET call to the URL specified in the 'close_url' config. Response from this URL must be of status code 200 and the body should contain ```{"state":0}``` for closed state (or ```{"state":1}``` for open state in case of failure).

04. This plugin doesn't detect the 'OPENING' and 'CLOSING' state. Instead it waits 15 seconds (by default) to simulate 'OPENING' and 'CLOSING' before changing to target state. You can change this wait time using the 'status_update_delay' config. 