var Service;
var Characteristic;
const request = require('request');

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory('homebridge-sbox-garagedoor', 'SboxGarageDoor', SboxGarageAccessory);
};

function SboxGarageAccessory(log, config) {
  this.log = log;
  this.name = config.name;
  this.openUrl = config.open_url;
  this.closeUrl = config.close_url;
  this.stateUrl = config.state_url;
  this.statusUpdateDelay = config.status_update_delay || 15;

  this.informationService = new Service.AccessoryInformation();
  this.informationService
  .setCharacteristic(Characteristic.Manufacturer, 'ACube')
  .setCharacteristic(Characteristic.Model, 'SboxGarage')
  .setCharacteristic(Characteristic.SerialNumber, '001');

  this.garageDoorService = new Service.GarageDoorOpener(this.name);
  this.garageDoorService.getCharacteristic(Characteristic.TargetDoorState)
  .on('set', this.setState.bind(this));

  if (this.stateUrl) {
    this.garageDoorService.getCharacteristic(Characteristic.CurrentDoorState)
    .on('get', this.getState.bind(this));
    this.garageDoorService.getCharacteristic(Characteristic.TargetDoorState)
    .on('get', this.getState.bind(this));
  }
}

SboxGarageAccessory.prototype.setState = function(isClosed, callback) {
  var me = this;
  var state = isClosed ? 'close' : 'open';
  var target = state + 'Url';
  var url = me[target];
  me.log('Url to call: ' + url);

  this.makeHttpRequest(url, function(error, response, body){
      if(error){
        me.log('Error setting ' + me.name + ' to ' + state);
        callback(error || new Error('Error setting ' + me.name + ' to ' + state));
      } else {
          var currentState = !!JSON.parse(body).state;
          var targetState = (currentState)? 'OPEN' : 'CLOSED'
          me.log('Set state: ' + state);

          if(targetState === 'OPEN'){
            me.garageDoorService.setCharacteristic(Characteristic.CurrentDoorState, Characteristic.CurrentDoorState.OPENING);  
            setTimeout(
              function() {
                me.garageDoorService.setCharacteristic(Characteristic.CurrentDoorState, Characteristic.CurrentDoorState.OPEN);
              },
              me.statusUpdateDelay * 1000
            );
        } else if (targetState === 'CLOSED') {
          me.garageDoorService.setCharacteristic(Characteristic.CurrentDoorState, Characteristic.CurrentDoorState.CLOSING);
          setTimeout(
            function() {
              me.garageDoorService.setCharacteristic(Characteristic.CurrentDoorState, Characteristic.CurrentDoorState.CLOSED);
            },
            me.statusUpdateDelay * 1000
          );
        }
        callback(null);
      }
  });
};

SboxGarageAccessory.prototype.getState = function(callback) {
  var me = this;
  this.makeHttpRequest(this.stateUrl, function(error, response, body){
      if(error){
          callback(error || new Error('Error getting state of ' + me.name));
      } else {
          var currentState = !!JSON.parse(body).state;
          var state = (currentState)? 'OPEN' : 'CLOSED'
          me.log('Current state: ' + state);
          callback(null, Characteristic.CurrentDoorState[state]);
      }
  });
};

SboxGarageAccessory.prototype.makeHttpRequest = function(url, callback) {
  var me = this;
  request({
          url: url,
          method: 'GET'
      }, 
      function(error, response, body) {
          if (response.statusCode !== 200){
              me.log('STATUS: ' + response.statusCode);
              me.log('HEADERS: ' + JSON.stringify(response.headers));
              me.log('BODY: ' + body);
          }
          return callback(error, response, body);
      }
  );
};


SboxGarageAccessory.prototype.getServices = function() {
  return [this.informationService, this.garageDoorService];
};
